/**
 * Here you can define helper functions to use across your app.
 */

export default function delay(seconds) {

    let prom = new Promise((resolve) => setTimeout(resolve, seconds * 1000));
    return prom;
}